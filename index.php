<?php
	echo 'Hello World (in PHP)';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
		<title>My website</title>

        <!-- ##### METAS ##### -->
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="title" content="My website">
		<meta name="description" content="My websitte description">
		<meta name="keywords" content="Site, Test">
		<meta name="language" content="English">
		<meta name="author" content="Natan FOURIÉ">
		
		<!-- ##### LINKS ##### -->
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="style.css">
	</head>

    <body>
        <p>Hello World (in HTML)</p>

		<!-- ##### SCRIPTS ##### -->
    </body>
</html>
